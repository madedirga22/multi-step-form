import 'package:flutter/material.dart';
import 'package:multi_stage_form/shared/theme.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Step> stepList() => [
        Step(
          title: Text(
            'Account',
            style: primaryTextStyle.copyWith(
              fontSize: 14,
              fontWeight: FontWeight.w600,
            ),
          ),
          content: const Center(
            child: Text('Account'),
          ),
        ),
        Step(
          title: Text(
            'Address',
            style: secondaryTextStyle.copyWith(
              fontSize: 14,
              fontWeight: FontWeight.w600,
            ),
          ),
          content: const Center(
            child: Text('Address'),
          ),
        ),
        Step(
          title: Text(
            'Confirm',
            style: secondaryTextStyle.copyWith(
              fontSize: 14,
              fontWeight: FontWeight.w600,
            ),
          ),
          content: const Center(
            child: Text('Confirm'),
          ),
        ),
      ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: const EdgeInsets.symmetric(vertical: 20.0),
        child: Stepper(
          type: StepperType.horizontal,
          steps: stepList(),
          elevation: 0,
        ),
      ),
    );
  }
}
