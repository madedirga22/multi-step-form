import 'package:flutter/material.dart';

double defaultMargin = 30.0;

Color primaryColor = const Color(0xffFFFFFF);
Color primaryTextColor = const Color(0xff0B0A0A);
Color secondaryTextColor = const Color(0xffBAB2B2);

TextStyle primaryTextStyle =
    TextStyle(fontFamily: "Quicksand", color: primaryTextColor);

TextStyle secondaryTextStyle =
    TextStyle(fontFamily: "Quicksand", color: secondaryTextColor);
